package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);

        // change it so that it's a different company

        Employee[] employees2 = new Employee[] { new Employee("Danny", 123456), new Employee("Gabriel", 124), new Employee("Andrew", 123)};

        Company c2 = new Company(employees2);
        assertNotEquals(c1, c2);

    }
}
